// ==== Challenge 1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by logging the car's year, make, and model in the console log provided to you below:
module.exports = function(inventory, id)
{
    let carnew;
    for (let i = 0; i < inventory.length; i++)
    {
        if (inventory[i].id === id)
        {
            carnew = inventory[i];
            break;
        }
    }
    let carfull = 'Car 33 is a ' + carnew.car_year + ' ' + carnew.car_make + ' ' + carnew.car_model;
    return carfull;
}
