module.exports = function(inventory)
{
    let sort = inventory.sort((a, b) => a.car_model > b.car_model ? 1 : -1);
    return sort;
}
