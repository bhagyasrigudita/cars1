module.exports = function(inventory)
{
    let BMWAudi = [];
    for (let i = 0; i < inventory.length; i++)
    {
        if ((inventory[i].car_make === 'BMW') || (inventory[i].car_make === 'Audi'))
        {
            BMWAudi.push(inventory[i]);
        }
    }
    return BMWAudi;
}
